CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Feature Boost theme is Mobile-friendly Drupal 9/10 responsive theme.
This theme features a custom Banner, Footercopyright  field, Social Menu Icon,
Contact us Information Fields, responsive layout, multiple column layouts and
is highly customizable.

Feature Boost Theme is developed with all latest technologies Drupal 9/10.

* For a full description of the theme, visit the project page:
https://www.drupal.org/project/feature_boost

* To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/feature_boost


 FEATURES
--------
 * Responsive, Mobile-Friendly Theme
 * Mobile support (Smartphone, Tablet, Android, iPhone, etc)
 * A total of 8 block regions
 * Custom Banner background
 * Sticky header
 * Drop Down main menus with toggle menu at mobile.
 * Gulp(SCSS) functionality


REQUIREMENTS
------------

This theme doesn't required anything from outside of Drupal core.


INSTALLATION
------------

 * Install the Feature Boost theme as you would normally install a
   contributed Drupal theme. Visit https://www.drupal.org/node/1897420 for
   further information.

 * To install the gulp use the below steps
  1) Need node version 14 to install the NPM.
  2) Run "nvm use 14"
  3) Run "npm install"
  4) To compile scss to css use gulp watch. 


CONFIGURATION
-------------

Navigate to Administration > Appearance and enable the theme.

Available options in the theme settings:

 * Hide/show banner in front page.
 * Responsive Sticky header for with transparent,
    if banner available on front page.
 * Hide/show and change copyright text.
 * Hide/show and change Social Media Icon.
 * Hide/show and change Information Field text.
 * Level 2 menus with responsive

MAINTAINERS
-----------

 * Ashutosh Ahirwal (ashutoshahirwal) - https://www.drupal.org/u/ashutosh-ahirwal
