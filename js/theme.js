(function ($, Drupal) {
  Drupal.behaviors.menu = {
    attach: function attach(context)
    {
      // document.addEventListener("DOMContentLoaded", function(){
      // // make it as accordion for smaller screens
      // if (window.innerWidth < 992) {
      
      //   // close all inner dropdowns when parent is closed
      //   document.querySelectorAll('.navbar .dropdown').forEach(function(everydropdown){
      //     everydropdown.addEventListener('hidden.bs.dropdown', function () {
      //       // after dropdown is hidden, then find all submenus
      //         this.querySelectorAll('.submenu').forEach(function(everysubmenu){
      //           // hide every submenu as well
      //           everysubmenu.style.display = 'none';
      //         });
      //     })
      //   });
      
      //   document.querySelectorAll('.dropdown-menu a').forEach(function(element){
      //     element.addEventListener('click', function (e) {
      //         let nextEl = this.nextElementSibling;
      //         if(nextEl && nextEl.classList.contains('submenu')) {	
      //           // prevent opening link if link needs to open dropdown
      //           e.preventDefault();
      //           if(nextEl.style.display == 'block'){
      //             nextEl.style.display = 'none';
      //           } else {
      //             nextEl.style.display = 'block';
      //           }
      
      //         }
      //     });
      //   })
      // }
      // // end if innerWidth
      // });
      
      $(document).ready(function() {
        // Add event handler for mouse enter (hover) on menu items
        $('.nav-item.dropdown').mouseenter(function() {
          // Add 'hovered' class to the hovered menu item and its dropdown menu
          $(this).addClass('hovered');
          // $(this).find('.dropdown-menu').addClass('hovered');
          $(this).children('.dropdown-menu').addClass('hovered');
        });
    
        // Add event handler for mouse leave
        $('.nav-item.dropdown').mouseleave(function() {
            // Remove 'hovered' class from the menu item and its dropdown menu
            $(this).removeClass('hovered');
            // $(this).find('.dropdown-menu').removeClass('hovered');
            $(this).children('.dropdown-menu').removeClass('hovered');
        });
      });
    
    }
  };
})(jQuery, Drupal);
